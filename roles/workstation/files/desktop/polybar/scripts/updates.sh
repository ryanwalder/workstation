#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

PENDING=$(cat /var/cache/pacman/pending-updates)

if [[ "${PENDING}" -gt 0 ]]; then
  if [[ "${PENDING}" -eq 1 ]]; then
    UPDATES="${PENDING} Update"
  else
    UPDATES="${PENDING} Updates"
  fi
else
  UPDATES="0"
fi

if [[ -f /var/cache/pacman/reboot-required ]]; then
  REBOOT=" "
else
  REBOOT=""
fi

echo "${UPDATES}${REBOOT}"

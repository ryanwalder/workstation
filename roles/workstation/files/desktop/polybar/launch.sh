#!/usr/bin/env bash
set -euox pipefail
IFS=$'\n\t'

killall -q polybar || echo 0

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
MAIN=$(xrandr --query | grep "primary" | cut -d" " -f1)
MONITOR="${MAIN}" polybar main &

for monitor in $(xrandr --query | grep " connected" | grep -v "primary" | cut -d" " -f1); do
  MONITOR="${monitor}" polybar side &
done

#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

ANNOUNCE_URL="${ANNOUNCE_URL:-${PTP_URL}}"

# Get the Filename (the largest mkv)
FILE=$(
	find . -type f -iname "*.mkv" -exec du -hs {} \; |
		sort -rh |
		head -n 1 |
		awk '{print $2}' |
		sed 's/\.\///'
)

# Remove the FILE extension
FILM="${FILE%.*}"

# Work out the size of the file
SIZE=$(stat "${FILE}" | grep "Size" | awk '{print $2}')

# Work out a decent piece size
if ((0 <= SIZE && SIZE <= 52428800)); then
	#Files up to 50MiB: 32KiB piece size
	PIECE_SIZE=32768
elif ((52428801 <= SIZE && SIZE <= 157286400)); then
	#Files 50MiB to 150MiB: 64KiB piece size
	PIECE_SIZE=65536
elif ((157286401 <= SIZE && SIZE <= 367001600)); then
	#Files 150MiB to 350MiB: 128KiB piece size
	PIECE_SIZE=131072
elif ((367001601 <= SIZE && SIZE <= 536870912)); then
	#Files 350MiB to 512MiB: 256KiB piece size
	PIECE_SIZE=262144
elif ((536870913 <= SIZE && SIZE <= 1073741824)); then
	#Files 512MiB to 1.0GiB: 512KiB piece size
	PIECE_SIZE=524288
elif ((1073741825 <= SIZE && SIZE <= 2147483648)); then
	#Files 1.0GiB to 2.0GiB: 1MiB piece size
	PIECE_SIZE=1048576
elif ((2147483648 <= SIZE && SIZE <= 4294967296)); then
	#Files 2.0GiB to 4.0GiB: 2MiB piece size
	PIECE_SIZE=2097152
elif ((4294967297 <= SIZE && SIZE <= 8589934592)); then
	#Files 4.0GiB to 8.0GiB: 4MiB piece size
	PIECE_SIZE=4194304
elif ((8589934593 <= SIZE && SIZE <= 17179869184)); then
	#Files 8.0GiB to 16.0GiB: 8MiB piece size
	PIECE_SIZE=8388608
elif [[ ${SIZE} -gt 17179869184 ]]; then
	#Files 16.0GiB and up: 16MiB piece size
	PIECE_SIZE=16777216
fi

# Create some screenshots every 10 mins
loop=1
for interval in 10 20 30 40 50; do
	ffmpeg -ss "00:${interval}:00" \
		-i "${FILE}" \
		-y \
		-vframes 1 \
		-vf "scale='max(sar,1)*iw':'max(1/sar,1)*ih'" \
		"${FILM}_${loop}.png" 2>/dev/null

	((loop += 1))
done

buildtorrent \
	--piecelength="${PIECE_SIZE}" \
	--quiet \
	--announce "${ANNOUNCE_URL}" \
	"${FILE}" \
	"${FILM}.torrent"

mediainfo "${FILE}"

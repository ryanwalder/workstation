""" Spelling
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red

"" disable cursor changing in nvim
set guicursor=
"" colours
set nohlsearch
syntax on
set t_Co=256
set background=light
highlight Comment ctermfg=blue
highlight Search ctermfg=256 ctermfg=0

set nocompatible
set backspace=2
set showmode
set nrformats-=octal
filetype plugin indent on
set nofoldenable
set mouse=
set clipboard=unnamedplus

set runtimepath+=/usr/share/vim/vimfiles

""""""" tabs and indents
set expandtab
set tabstop=2
set shiftwidth=2

" tabs and indents per file type
autocmd Filetype python setlocal ts=4 sts=4 sw=4

" disable ex mode
nnoremap Q <Nop>

""""""" binds
let mapleader = "\<Space>"

" whitespace
map <F7> mzgg=G`z<CR>
map <F8> :StripWhitespace<CR>
map <F12> :set spell!<CR>
inoremap <S-Tab> <C-V><Tab>

" insert date
nmap <Leader>d a<C-R>=strftime("%Y-%m-%d")<CR><Esc>
" sort
vnoremap <Leader>s :sort<CR>
vnoremap <Leader>i !sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4<CR>

" easier code indenting
vnoremap < <gv
vnoremap > >gv

" generate 128char string
nnoremap <F6> :r! pwgen 128 -s -1<CR>kJ

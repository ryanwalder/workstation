-- Import Lua modules
require('packer_init')
require('core/options')
require('core/colors')
require('core/autocmds')
require('core/keymaps')
require('plugins/nvim-cmp')
require('plugins/nvim-lspconfig')
require('plugins/nvim-treesitter')
require('plugins/nvim-lualine')
require('plugins/terraform')

-- Import snippets
require("luasnip.loaders.from_snipmate").lazy_load()

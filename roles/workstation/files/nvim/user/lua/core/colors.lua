-----------------------------------------------------------
-- Color scheme configuration
-----------------------------------------------------------

vim.opt.background = 'dark'

-- Load nvim color scheme:
-- Change the "require" values with your color scheme
local status_ok, color_scheme = pcall(require, 'onedark')
if not status_ok then
  return
end

require('onedark').setup {
  style = 'deep',
  transparent = true,
}
require('onedark').load()

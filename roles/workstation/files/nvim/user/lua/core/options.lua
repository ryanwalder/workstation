-----------------------------------------------------------
-- General Neovim settings and configuration
-----------------------------------------------------------

-- Default options are not included
-- See: https://neovim.io/doc/user/vim_diff.html
-- [2] Defaults - *nvim-defaults*

local g = vim.g       -- Global variables
local opt = vim.opt   -- Set options (global/buffer/windows-scoped)

-----------------------------------------------------------
-- Directories
-----------------------------------------------------------
opt.backup    = true
opt.undofile  = true

opt.directory = os.getenv("HOME") .. '/.local/share/nvim/swap'
opt.backupdir = os.getenv("HOME") .. '/.local/share/nvim/backup'
opt.undodir   = os.getenv("HOME") .. '/.local/share/nvim/undo'

-----------------------------------------------------------
-- General
-----------------------------------------------------------
opt.mouse       = ''                          -- Enable mouse support
opt.clipboard   = 'unnamedplus'               -- Copy/paste to system clipboard
opt.completeopt = 'menuone,noinsert,noselect' -- Autocomplete options

-----------------------------------------------------------
-- Neovim UI
-----------------------------------------------------------
opt.showmatch     = true       -- Highlight matching parenthesis
opt.foldmethod    = 'marker'   -- Enable folding (default 'foldmarker')
opt.ignorecase    = true       -- Ignore case letters when search
opt.smartcase     = true       -- Ignore lowercase for the whole pattern
opt.linebreak     = true       -- Wrap on word boundary
-- opt.termguicolors = true       -- Enable 24-bit RGB colors
opt.laststatus    = 3          -- Set global statusline

-----------------------------------------------------------
-- Tabs, indent
-----------------------------------------------------------
opt.expandtab   = true         -- Use spaces instead of tabs
opt.shiftwidth  = 2            -- Shift 2 spaces when tab
opt.tabstop     = 2            -- 1 tab == 2 spaces
opt.smartindent = true         -- Autoindent new lines

-----------------------------------------------------------
-- Memory, CPU
-----------------------------------------------------------
opt.hidden     = true       -- Enable background buffers
opt.history    = 100        -- Remember N lines in history
opt.lazyredraw = true       -- Faster scrolling
opt.synmaxcol  = 240        -- Max column for syntax highlight
opt.updatetime = 700        -- ms to wait for trigger an event

-----------------------------------------------------------
-- Startup
-----------------------------------------------------------
-- Disable nvim intro
opt.shortmess:append "sI"

-- Disable builtins plugins
local disabled_built_ins = {
  "gzip",
  "zip",
  "zipPlugin",
  "tar",
  "tarPlugin",
  "getscript",
  "getscriptPlugin",
  "vimball",
  "vimballPlugin",
  "2html_plugin",
  "logipat",
  "rrhelper",
  "spellfile_plugin",
  "matchit"
}

for _, plugin in pairs(disabled_built_ins) do
  g["loaded_" .. plugin] = 1
end

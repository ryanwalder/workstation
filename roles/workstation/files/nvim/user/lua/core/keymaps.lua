----------------------------------------------------------
-- Define keymaps of Neovim and installed plugins.
-----------------------------------------------------------

local function map(mode, lhs, rhs, opts)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- Change leader to space
vim.g.mapleader = '<Space>'

-----------------------------------------------------------
-- Neovim shortcuts
-----------------------------------------------------------

-- Clear search highlighting with <leader> and c
map('n', '<leader>c', ':nohl<CR>')

-- Toggle auto-indenting for code paste
map('n', '<F2>', ':set invpaste paste?<CR>')
vim.opt.pastetoggle = '<F2>'

-- Reload configuration without restart nvim
map('n', '<leader>r', ':so %<CR>')

-- Fast saving with <leader> and s
map('n', '<leader>s', ':w<CR>')
map('i', '<leader>s', '<C-c>:w<CR>')

-- Close all windows and exit from Neovim with <leader> and q
map('n', '<leader>q', ':qa!<CR>')

-- Better indenting
map('v', '>', '>gv')
map('v', '<', '<gv')

-- Sort Alphabetically
map('v', '<leader>s', ':sort<CR>')

-- Sort IP Addresses
map('v', '<leader>i', ':sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4<CR>')

-----------------------------------------------------------
-- Applications and Plugins shortcuts
-----------------------------------------------------------

-- MarkdownPreview: start
map('n', '<F10>', ':MarkdownPreview<CR>')

-- Commentary: Comment visual block
map('v', '#', ':Commentary<CR>')
map('n', '#', ':Commentary<CR>')

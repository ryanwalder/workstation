requirements:
	@ansible-galaxy collection install --upgrade -r requirements.yml

bootstrap:
	@sudo pacman -Syyu ansible

blackout:
	@ansible-playbook \
		--extra-vars @vars/all.yml \
		--extra-vars @vars/home.yml \
		--extra-vars @vars/blackout.yml \
		--extra-vars ansible_become_pass='{{ lookup("env", "ANSIBLE_BECOME_PASS") }}' \
		workstation.yml

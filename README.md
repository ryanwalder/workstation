# Workstation Playbook

Playbook/Role for setting up my workstations.

Extremely Arch centric, won't work with other distros without a lot of changes.

As per recommended Arch usage this playbook will do a full system update near the beginning of each run

Note: This repo uses `hash_behaviour = merge` which means lists/dicts variables will get merged, this is not recommended Ansible usage but suits my needs.

## Usage

As root:

0. Install Arch as usual
0. Create a user
0. Add user to `wheel` group/Add to sudoers
0. `pacman -Syyu git make`

As user:

0. Clone this repo
0. Edit variables in `vars/` as needed
    * Update `user` key as a minimum
0. Add new makefile target that loads the var files you want if needed
0. `make bootstrap` to install Ansible
0. `make requirements` to install Ansible requirements
0. Export the below variables, after the first run we'll use [direnv](https://direnv.net/) to populate these.
    * `ANSIBLE_BECOME_PASS`
    * `ANSIBLE_VAULT_PASS`
0. `make $machinename`
0. `sudo reboot` if first run from a tty

Note: Firefox will not be installed/configured if run from a non graphical session as it needs to launch during the install/configuration. Simply re-run the playbook once you're in a graphical session to have it install.
